import java.util.GregorianCalendar;

/*+----------------------------------------------------------------------
 ||
 ||  Class Email
 ||
 ||         Author:  Michael Humphrey
 ||
 ||        Purpose:  This class simply holds the information important to sorting an email.
 ||					 it contains the email address of the sender, date it was sent and the message sent.
 ||					 This aids in sorting because the information can be stored and retrieved easily.
 ||
 ||  Inherits From:  None.
 ||
 ||     Interfaces:  None.
 ||
 |+-----------------------------------------------------------------------
 ||
 ||      Constants:  None.
 ||
 |+-----------------------------------------------------------------------
 ||
 ||   Constructors:  
 ||					 public Email(String email, GregorianCalendar date, String message)
 ||					 public Email()
 ||
 ||  Class Methods:  
 ||					 public String getMessage()
 ||					 public String setMessage()
 ||
 ||  Inst. Methods:  
 ||					 public String getMessage(): String
 ||					 public String setMessage(String message): void
 ||					 public String getDate(): GregorianCalendar
 ||					 public String setDate(GregorianCalendar date): void
 ||					 public String getEmail(): String
 ||					 public String setEmail(String email): void
 ||
 ++-----------------------------------------------------------------------*/
public class Email {

	private String message; // the contents of the email message
	private GregorianCalendar date; // the date represented as a GregorianCalendar object
	private String email; // the sender's email address

	public Email(String email, GregorianCalendar date, String message) {
		this.message = message;
		this.date = date;
		this.email = email;
	}

	public Email() {
		this.message = null;
		this.date = null;
		this.email = null;
	}

	/*---------------------------------------------------------------------
	|  Method Getters/Seeters
	|
	|  Purpose: The remaining methods in this calls are all getters/setters for this class/
	|
	|  Returns:  The instance variable for getters in this class.
	 *-------------------------------------------------------------------*/
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public GregorianCalendar getDate() {
		return date;
	}

	public void setDate(GregorianCalendar date) {
		this.date = date;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
