import java.util.GregorianCalendar;
import java.util.List;

/*+----------------------------------------------------------------------
||
||  Class MySort
||
||         Author:  Michael Humphrey
||
||        Purpose:  This class contains the sorts that were designed to sort the emails by date and sender.
||					It uses a modified bubble sort that not only looks from the beginning but switches to the end
||					when a swap has occured during that iteration of the loop. This way the data is sorted more quickly, because the second loop
||					iterates from the backend of the data and makes swaps as necessary. This approach works well with nearly sorted data.
||
||  Inherits From:  None.
||
||     Interfaces:  None.
||
|+-----------------------------------------------------------------------
||
||      Constants:  None.
||
|+-----------------------------------------------------------------------
||
||   Constructors:  This is a static class with no constructors.
||
||  Class Methods:  
||					sort(List<GregorianCalendar> list): void
||					sortString(List<String> list): void
||					shellSort(List<String> list): void
||
||  Inst. Methods:  None.
||
++-----------------------------------------------------------------------*/
public class MySort {

	
	/*---------------------------------------------------------------------
    |  Method sort
    |
    |  Purpose:  This method sorts a list by date which is represented by a Gregorian Calendar.
    |
    |  Pre-condition:  None.
    |
    |  Post-condition: None.
    |
    |  Parameters:
    |      List<GregorianCalendar> list - the list to be sorted
    |
    |  Returns:  	   None.
    *-------------------------------------------------------------------*/
	public static void sort(List<GregorianCalendar> list) {
		int begin = 0; // marks the beginning of the list
		int end = list.size() - 1; // marks the end of the list
		boolean swapped = true; // boolean that marks if element was swapped

		while (begin < end && swapped) {
			swapped = false;
			for (int k = begin; k < end; k++) {
				// test to see if k is after k + 1 if this is the case they need be switch
				if ((list.get(k).compareTo(list.get(k + 1)) > 0)) {
					GregorianCalendar temp = list.get(k);
					list.set(k, list.get(k + 1));
					list.set(k + 1, temp);
					swapped = true;
				}
			}
			end--;
			if (swapped) {
				swapped = false;
				for (int k = end; k > begin; k--) {
					if ((list.get(k).compareTo(list.get(k - 1)) < 0)) {
						GregorianCalendar temp = list.get(k);
						list.set(k, list.get(k - 1));
						list.set(k - 1, temp);
						swapped = true;
					}
				}
			}
			begin++;
		}
	}

	/*---------------------------------------------------------------------
    |  Method sort
    |
    |  Purpose:  This method sorts a list by email which is represented by a String.
    |
    |  Pre-condition:  None.
    |
    |  Post-condition: None.
    |
    |  Parameters:
    |      List<GregorianCalendar> list - the list to be sorted
    |
    |  Returns:  	   None.
    *-------------------------------------------------------------------*/
	public static void sortString(List<String> list) {
		int begin = 0;
		int end = list.size() - 1;
		boolean swapped = true;

		while (begin < end && swapped) {
			swapped = false;
			for (int k = begin; k < end; k++) {
				// test to see if k is after k + 1 if this is the case they need be switch
				if ((list.get(k).compareTo(list.get(k + 1)) > 0)) {
					String temp = list.get(k);
					list.set(k, list.get(k + 1));
					list.set(k + 1, temp);
					swapped = true;
				}
			}
			end--;
			if (swapped) {
				swapped = false;
				for (int k = end; k > begin; k--) {
					if ((list.get(k).compareTo(list.get(k - 1)) < 0)) {
						String temp = list.get(k);
						list.set(k, list.get(k - 1));
						list.set(k - 1, temp);
						swapped = true;
					}
				}
			}
			begin++;
		}
	}

	/*---------------------------------------------------------------------
    |  Method [Method Name]
    |
    |  Purpose:  [Explain what this method does to support the correct
    |      operation of its class, and how it does it.]
    |
    |  Pre-condition:  [Any non-obvious conditions that must exist
    |      or be true before we can expect this method to function
    |      correctly.]
    |
    |  Post-condition: [What we can expect to exist or be true after
    |      this method has executed under the pre-condition(s).]
    |
    |  Parameters:
    |      parameter_name -- [Explanation of the purpose of this
    |          parameter to the method.  Write one explanation for each
    |          formal parameter of this method.]
    |
    |  Returns:  [If this method sends back a value via the return
    |      mechanism, describe the purpose of that value here, otherwise
    |      state 'None.']
    *-------------------------------------------------------------------*/
	public static void shellSort(List<String> list) {

		int gaps[] = { 701, 301, 132, 57, 23, 10, 4, 1 }; // best known gaps
		int i; // loop control
		int j; // loop control

		// for each gap
		for (int gap : gaps) {
			for (i = gap; i < list.size(); i++) {
				String temp = list.get(i);
				for (j = i; j >= gap
						&& ((list.get(j - gap).compareTo(temp)) > 0); j -= gap) {
					list.set(j, list.get(j - gap));
				}
				list.set(j, temp);
			}
		}

	}

}
