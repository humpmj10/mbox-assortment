import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Scanner;

/*=============================================================================
 |   Assignment:  Program #[3]:  An mbox Assortment
 |       Author:  Michael Humphrey (michaeljhumphrey@email.arizona.edu)
 |       Grader:  Lisa Peairs
 |
 |       Course:  CSC 345
 |   Instructor:  L. McCann
 |     Due Date:  October 31st, 2013 1400 Hours
 |
 |  Description:  The goal of this program is to read a given file that contains
 |				  unsorted Emails and to sort these in a number of ways. It will
 |				  contain two different methods of sorting, the first will sort by
 |				  the date of the email and the second method will sort by the sender's
 |				  email address. The email that have been sorted by sender will also be
 |				  sorted by date. All date sorting is done in ascending order, so from the oldest email to th
 |				  most recent. There will be two algorithms to sort these email's,
 |				  the first being the Collections.sort, provided by the java api, and the
 |				  second will be one written by the programmer. Two files will be created, 
 |				  one containing the java collections sort and one containing the original algorithm's
 |				  sorts. 
 |                
 | Deficiencies:  Currently two minor errors exist. The output files are not exactly the same
 |				  as the input file, the outfiles are missing some new lines. Also the sort I designed
 |				  only beats the collections.sort when the number of emails to sort is large.
 *===========================================================================*/

public class Prog3 {

	static String fileName; // the name of the input file
	static Scanner scan; // will be used to scan the files
	private static String baseDir = System.getProperty("user.dir")
			+ File.separator + "textDocs" + File.separator; // ensures that program can locate the file on the disk regardless of which os it's being run on

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length == 2) {
			ArrayList<StringBuilder> emails = new ArrayList<StringBuilder>(); // will contain each email string, starting with From and ending with a blank line
			fileName = args[0]; // contains file from command line arg
			File file = new File(fileName); // the file that will be opened
			boolean firstLine = true; // contains boolean to test if text file is the first line 
			BufferedReader br = null; // buffered reader object taht will read the input stream
			String currentLine; // current line being read

			try {
				br = new BufferedReader(new FileReader(file)); // fileReader wrapped with bufferedReader to read in the file
				StringBuilder currentMessage = new StringBuilder(); // will hold the string for the current message

				// Read the file line by line, once scanner reaches a new message From_, build a new string for that message 
				while ((currentLine = br.readLine()) != null) {

					if (currentLine.startsWith("From ") && !firstLine) {
						emails.add(currentMessage); // add message to arrayList of email
						currentMessage = new StringBuilder(); // clear out the old message
						currentMessage.append(currentLine
								+ System.lineSeparator());
					} else {
						currentMessage.append(currentLine
								+ System.lineSeparator());
						firstLine = false;
					}

				}
				emails.add(currentMessage); // file has ended, need to add the last message

			} catch (IOException e) {
				System.out
						.println("The file was not found. Program will terminate.");
				System.exit(1);
			}

			// choose type of sort based on the command line arg
			if (args[1].equals("sender")) {
				sortByEmail(emails);
			} else if (args[1].equals("date"))
				sortByDate(emails);
			else {
				System.out.println("Usage: Prog3 [file path] [sender|date]");
				System.exit(1);
			}

		} else {
			System.out.println("Usage: Prog3 [file path] [sender|date]");
			System.exit(1);
		}

	}

	/*---------------------------------------------------------------------
	|  Method sortByDate
	|
	|  Purpose:  This method is called when the user selects sort by date from the command line.
	|			 It is responsible for email list that main passed to it and calling several methods to sort the data.
	|			 It first extracts the dates from the email by calling extract date and then calls
	|			 collections/user sort on these lists. Once the list has been generated the method calls createfile to make the output files.
	|
	|  Pre-condition: None.
	|
	|  Post-condition: None.
	|
	|  Parameters:
	|    		 ArrayList<StringBuilder> email - this is the list of emails to be sorted
	|
	|  Returns:  None.
	 *-------------------------------------------------------------------*/
	private static void sortByDate(ArrayList<StringBuilder> email) {
		ArrayList<Email> unsortedList = new ArrayList<Email>(); // this will be the unsorted list of email objects 
		ArrayList<GregorianCalendar> sortedListOfDatesUser = new ArrayList<GregorianCalendar>(); // this will be the unsorted list of dates and passed to user sort
		ArrayList<GregorianCalendar> sortedListOfDates = new ArrayList<GregorianCalendar>(); // this will be the unsorted list of dates and passed to collections sort
		ArrayList<Email> sortedList = new ArrayList<Email>(); // this will be the collections sorted list of emails and passed to create the output file
		ArrayList<Email> sortedListUser = new ArrayList<Email>(); // this will be the user sorted list of emails and passed to create the output file
		long estimatedTime; // will be the time taken by the algorithm to sort
		long startTime; // will be the start time for the sort
		float timeInSecs; // the converted time from nanosecs that the algorithm took to sort

		unsortedList = extractDate(email);

		// get the dates from the Email object so that they can be sorted
		for (Email currentEmail : unsortedList) {
			sortedListOfDates.add(currentEmail.getDate());
			sortedListOfDatesUser.add(currentEmail.getDate());
		}

		// sort the dates, both with the collections sort an user sort, also output time for sort
		startTime = System.nanoTime();
		Collections.sort(sortedListOfDates);
		estimatedTime = System.nanoTime() - startTime;
		timeInSecs = estimatedTime / 1000000000f;
		System.out.printf("The java sort took %.3f seconds\n", timeInSecs);

		startTime = System.nanoTime();
		MySort.sort(sortedListOfDatesUser);
		estimatedTime = System.nanoTime() - startTime;
		timeInSecs = estimatedTime / 1000000000f;
		System.out.printf("The user sort took %.3f seconds\n", timeInSecs);

		// The following two for loops use the sorted list of dates to create a sorted List of Emails which will be used to 
		// create new files with the sorted emails
		for (GregorianCalendar date : sortedListOfDates) {
			for (Email currentEmail : unsortedList) {
				if (currentEmail.getDate().equals(date)) {
					sortedList.add(currentEmail);
					currentEmail = null;
					break;
				}
			}
		}

		for (GregorianCalendar date : sortedListOfDatesUser) {
			for (Email currentEmail : unsortedList) {
				if (currentEmail.getDate().equals(date)) {
					sortedListUser.add(currentEmail);
					currentEmail = null;
					break;
				}
			}
		}
		// create new file with the now sorted list
		createNewFile(sortedList, true);
		createNewFile(sortedListUser, false);

	}

	/*---------------------------------------------------------------------
	|  Method sortByEmail
	|
	|  Purpose:  This method is called when the user selects sort by email from the command line.
	|			 It is responsible for taking the email list that main passed to it and calling several methods to sort the data.
	|			 It first extracts the dates and emailAddresses from the email by calling extract date and extract email. It then calls
	|			 collections/user sort on these lists. Once the list has been generated the method calls createfile to make the output files.
	|
	|  Pre-condition:  None.
	|
	|  Post-condition: None.
	|
	|  Parameters:
	|     		 ArrayList<StringBuilder> email - this is the list of emails to be sorted
	|
	|  Returns:  None.
	 *-------------------------------------------------------------------*/
	private static void sortByEmail(ArrayList<StringBuilder> email) {

		ArrayList<Email> unsortedList = new ArrayList<Email>(); // this will be the unsorted list of email objects that contain it's date, email and message
		ArrayList<Email> unsortedListEmail = new ArrayList<Email>(); // the list of emails extracted from the email objects
		ArrayList<GregorianCalendar> sortedListDatesUser = new ArrayList<GregorianCalendar>(); // the sorted list of dates after collections sort
		ArrayList<GregorianCalendar> sortedListDates = new ArrayList<GregorianCalendar>(); // the sorted list of dates after user sort
		ArrayList<String> sortedListEmails = new ArrayList<String>(); // the sorted list of emails after collections sort
		ArrayList<String> sortedListEmailsUser = new ArrayList<String>(); // the sorted list of emails after user sort
		ArrayList<Email> sortedList = new ArrayList<Email>(); // the sorted list of email objects after collections sort
		ArrayList<Email> sortedListUser = new ArrayList<Email>(); // the sorted list of email objects after user sort
		ArrayList<Email> temp = new ArrayList<Email>(); // used during the copying of arrays

		long estimatedTime; // will be the time taken by the algorithm to sort
		long startTime; // will be the start time for the sort
		float timeInSecs; // the converted time from nanosecs that the algorithm took to sort

		// get the unsorted emails as keys
		unsortedList = extractDate(email);
		unsortedListEmail = extractEmail(email);

		for (int i = 0; i < email.size(); i++) {
			// get the date from unsortedList dates and add to the already created email objects that have the emails
			unsortedList.get(i).setEmail(unsortedListEmail.get(i).getEmail());
		}

		// loop to extract emails and dates which will used as keys for the sort
		for (Email currentEmail : unsortedList) {
			sortedListDatesUser.add(currentEmail.getDate());
			sortedListDates.add(currentEmail.getDate());
			sortedListEmails.add(currentEmail.getEmail());
			sortedListEmailsUser.add(currentEmail.getEmail());
		}

		// sort the email address and dates, output time taken for collections sort
		startTime = System.nanoTime();
		Collections.sort(sortedListDates);
		Collections.sort(sortedListEmails);
		estimatedTime = System.nanoTime() - startTime;
		timeInSecs = estimatedTime / 1000000000f;
		System.out.printf("The java sort took %.3f seconds\n", timeInSecs);

		// sort the email address and dates, output time taken for user sort
		startTime = System.nanoTime();
		MySort.sort(sortedListDatesUser);
		MySort.sortString(sortedListEmailsUser);
		estimatedTime = System.nanoTime() - startTime;
		timeInSecs = estimatedTime / 1000000000f;
		System.out.printf("My sort took %.3f seconds\n", timeInSecs);

		/*
		 * The following two for loops use the sorted keys, dates, to find the
		 * correct emails and add them to the sorted list emails. This sorting
		 * of dates first will allow the emails to be sorted after the
		 * collections sort has finished because it is a stable sort. The first
		 * loop compares dates from the sorted keys with the unsorted list of
		 * emails. Once it finds the correct email it adds this email object to
		 * the sorted array of email objects. The second loop will continue this
		 * process by looking for the correct email as the key. Once either of
		 * these loops finds the object it makes the sortedlist object null to
		 * avoid duplicates.
		 */
		Email current;
		temp.addAll(unsortedList);
		for (GregorianCalendar date : sortedListDates) {
			for (int i = 0; i < temp.size(); i++) {
				if (temp.get(i) != null) {
					current = temp.get(i);
				} else {
					continue;
				}
				if (current.getDate().equals(date)) {
					sortedList.add(current);
					temp.set(i, null);
					break;
				}
			}
		}
		temp.clear();
		temp.addAll(sortedList);
		sortedList.clear();
		for (String emailAddress : sortedListEmails) {
			for (int i = 0; i < temp.size(); i++) {
				if (temp.get(i) != null) {
					current = temp.get(i);
				} else
					continue;
				if (current.getEmail().equals(emailAddress)) {
					sortedList.add(current);
					temp.set(i, null);
					break;
				}
			}
		}
		/*
		 * The following two for loops use the sorted keys, dates, to find the
		 * correct emails and add them to the sorted list emails. This sorting
		 * of dates first will allow the emails to be sorted after the user sort
		 * has finished because it is a stable sort. The first loop compares
		 * dates from the sorted keys with the unsorted list of emails. Once it
		 * finds the correct email it adds this email object to the sorted array
		 * of email objects. The second loop will continue this process by
		 * looking for the correct email as the key. Once either of these loops
		 * finds the object it makes the sortedlist object null to avoid
		 * duplicates.
		 */
		temp.addAll(unsortedList);
		for (GregorianCalendar date : sortedListDatesUser) {
			for (int i = 0; i < temp.size(); i++) {
				if (temp.get(i) != null) {
					current = temp.get(i);
				} else
					continue;
				if (current.getDate().equals(date)) {
					sortedListUser.add(current);
					temp.set(i, null);
					break;
				}
			}
		}
		temp.clear();
		temp.addAll(sortedListUser);
		sortedListUser.clear();
		for (String emailAddress : sortedListEmailsUser) {
			for (int i = 0; i < temp.size(); i++) {
				if (temp.get(i) != null) {
					current = temp.get(i);
				} else
					continue;
				if (current.getEmail().equals(emailAddress)) {
					sortedListUser.add(current);
					temp.set(i, null);
					break;
				}
			}
		}

		// create new file with the now sorted list
		createNewFile(sortedList, true);
		createNewFile(sortedListUser, false);

	}

	/*---------------------------------------------------------------------
	|  Method createNewFile
	|
	|  Purpose:  This method is reponsible for creating new output files with the sorted email message printed.
	|			 The new files will contain a -java extension if they were sorted by collections.sort or a 
	|			 -mine extension if sorted by the programmers sorting algorithm. 
	|
	|  Pre-condition:	None.
	|
	|  Post-condition: 	None.
	|
	|  Parameters:
	|      	ArrayList<Email> email - the sorted list of emails to be written to the new file
	|		boolean java - this boolean will indicate if the java or programmers algorithm was used to sort
	|
	|  Returns: 		None.
	 *-------------------------------------------------------------------*/
	private static void createNewFile(ArrayList<Email> email, boolean java) {
		BufferedWriter writer; // will write out to file
		File file; // the new output file
		// Create file with the extension -java
		if (java) {
			file = new File(fileName + "-java");
		} else {
			file = new File(fileName + "-mine");
		}
		try {
			writer = new BufferedWriter(new FileWriter((file)));
			// for each email in the sorted list, write to the file 
			for (Email currentEmail : email) {
				writer.write(currentEmail.getMessage().toString());
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*---------------------------------------------------------------------
	|  Method extractDate
	|
	|  Purpose:  This method extracts the date from the list of email StringBuilder objects.
	|			 It then adds the dates to the correct field in the Email object. This is needed
	|            for quick access and storage in the sorting process. The date is represent with the GregorianCalendar object
	|			 and contains, the year, month, day, hour, min, sec of the email time.
	|
	|  Pre-condition: 	None.
	|
	|  Post-condition: 	None.
	|
	|  Parameters:
	|      ArrayList<StringBuilder> email - the arrayList that contains all the email messages as StringBuilder objects
	|
	|  Returns:  Returns and ArrayList of Email objects that contain the dates as fields.
	 *-------------------------------------------------------------------*/
	public static ArrayList<Email> extractDate(ArrayList<StringBuilder> email) {

		ArrayList<Email> result = new ArrayList<Email>(); // will hold the dates objects as keys and email messages as associated date
		GregorianCalendar temp; // will represent the date for each email
		ArrayList<String> months = buildMonthArray(); // needed for array index of months
		int month; // variable for calendar
		int day; // variable for calendar
		int year; // variable for calendar
		int hour; // variable for calendar
		int mins; // variable for calendar
		int secs; // variable for calendar
		String time; // holds the time as a string
		Email tempEmail; // holds the temp email object before it's added to the arrayList

		for (StringBuilder currentEmail : email) {

			tempEmail = new Email();
			scan = new Scanner(currentEmail.toString());

			// scan to the first portion of date
			for (int i = 0; i < 3; i++) {
				scan.next();
			}

			// find each field for Gregorian Calandar
			month = months.indexOf(scan.next());
			day = scan.nextInt();
			time = scan.next();
			// the time is a long string, it needs to be broken up and parsed
			hour = Integer.parseInt(time.substring(0, 2));
			mins = Integer.parseInt(time.substring(3, 5));
			secs = Integer.parseInt(time.substring(6));
			year = scan.nextInt();

			// set the calander object and add to hashtable
			temp = new GregorianCalendar(year, month, day, hour, mins, secs);

			// set message and date object
			tempEmail.setDate(temp);
			tempEmail.setMessage(currentEmail.toString());

			// add to the return arrayList
			result.add(tempEmail);

		}
		scan.close();

		return result;
	}

	/*---------------------------------------------------------------------
	|  Method extractEmail
	|
	|  Purpose:  This method extracts the email address from the list of email StringBuilder objects.
	|			 It then adds the emails to the correct field in the Email object. This is needed
	|            for quick access and storage in the sorting process.
	|
	|  Pre-condition: 	None.
	|
	|  Post-condition: 	None.
	|
	|  Parameters:
	|      ArrayList<StringBuilder> email - the arrayList that contains all the email messages as StringBuilder objects
	|
	|  Returns:  Returns and ArrayList of Email objects that contain the emailAddresses as fields.
	 *-------------------------------------------------------------------*/
	public static ArrayList<Email> extractEmail(ArrayList<StringBuilder> email) {
		ArrayList<Email> result = new ArrayList<Email>(); // the resulting array containing a list of email objects
		Email tempEmail; // a temp email used when inserting the new emails into array

		// go through email messages and extract the sender's email addresses, place them in sortedList
		for (StringBuilder sendersEmail : email) {
			tempEmail = new Email();
			scan = new Scanner(sendersEmail.toString());
			scan.next();
			tempEmail.setEmail(scan.next());
			tempEmail.setMessage(sendersEmail.toString());
			result.add(tempEmail);
		}
		scan.close();

		return result;
	}

	/*---------------------------------------------------------------------
	|  Method buildMonthArray
	|
	|  Purpose:  The emails given to be sorted all contain the first three letters of a month to represent that month.
	|			 This method builds an array with all the three letter months as strings at the correct index. Jan - 0 through Dec - 11.
	|			 The method aids the class is finding the correct month so that the GregorianCalendar can be built for sorting.
	|
	|  Pre-condition:  None.
	|  Post-condition: None.
	|
	|  Parameters:
	|      			   None.
	|
	|  Returns:  
	|				   None.
	 *-------------------------------------------------------------------*/
	public static ArrayList<String> buildMonthArray() {
		ArrayList<String> result = new ArrayList<String>(); // stores the resulting alphabet array

		// load from a file the alphabet
		String file = "months.txt"; // stores the file name 
		Scanner scan; // used to scan the file

		try {
			scan = new Scanner(new File(baseDir + file));
			while (scan.hasNext()) {
				result.add(scan.next());
			}
			scan.close();
		} catch (FileNotFoundException e) {

		}
		return result;
	}
}
